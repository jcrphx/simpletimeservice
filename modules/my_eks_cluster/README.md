#  Files roles

* Kubernetes.tf (Main terraform Configuration File)
* eks-cluster.tf (Provision all resource like autosacaling group, cluster etc)
* segurity-groups.tf (Define all security groups)
* Versions.tf (Provide the version of the provider used)
* vpc.tf (All about networking)
* outputs.tf (Contain the output variables)