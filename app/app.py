
from flask import Flask, jsonify, request
from datetime import datetime

app = Flask(__name__)
# Disabling the jsonif alphabetic sorting feature, to return the data in the order requested by the task
app.config['JSON_SORT_KEYS'] = False

@app.route("/")
def home():
    now = datetime.now()
    # formating the date and time
    formatted_now = now.strftime("%d %B, %Y at %X")
    # storing the date/time data to be send in the response  
    content = formatted_now
    return jsonify({'timestamp': content, 'ip': request.remote_addr}), 200
    
# create the main driver function
if __name__=='__main__':
 app.run()