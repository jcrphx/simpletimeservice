
# Overview

This demo has been created in response to two challenging tasks for software engineers interested in DevOps positions. The tasks are designed to 
measure abilities in different technologies and concepts such as:

* Software Development in general by creating an extremely minimal service
* Docker Container
* Kubernetes from AWS cloud native flavor of kubernetes (EKS)
* Terraform writing modules and using these for deploy infrastructure 
* Infrastructure as a code 

# Toolset:
* OS platform:  Windows 10 pro supporting Windows Subsystem for Linux (WSL2) running Ubuntu v20.04 
* Python v3.10.1
	* Flask module 2.0.2 
* Docker engine v20.10.11
* Minikube v1.22.0
    * kubectl v1.21.2
* Terraform v1.1.0
* Docker Hub (jcrphx0330/simpletimeservice:v1.0.0)

# Task 1 - Minimalist Application Development / Docker / Kubernetes
## Tiny App Development: 'SimpleTimeService'

- Create a simple microservice (which we will call "SimpleTimeService") in any programming language of your choice: NodeJS, Python, C#, Ruby, whatever you like.  
- The application should be a web server that returns a pure JSON response with the following structure, when its `/` URL path is accessed:

```json
{
  "timestamp": "<current date and time>",
  "ip": "<the IP address of the visitor>"
}
```

Python application: 
```json
/simpletimeservice/app/app.py
```

## Dockerize SimpleTimeService

- Create a Dockerfile for this microservice.
- Your application must be configured to run as a non-root user in the container.
- Publish the image to a public DockerHub repo so we can pull it for testing.

Dockerfile file: 
```sh
/simpletimeservice/app/Dockerfile
```

## Create a k8s manifest for SimpleTimeService

- Create a Kubernetes manifest in YAML format, containing a Deployment and a Service, to deploy your microservice on Kubernetes. 
- Your Deployment must use your public Docker image from DockerHub.

Kubernetes manifest file: 
```sh
/simpletimeservice/app/k8/simpletimeservice.yml
```
## Push your code to a public git repo

## Acceptance Criteria

Your task will be considered successful if a colleague is able to deploy your manifests to a running Kubernetes cluster and use your microservice.   We will use Docker Desktop to test this exercise.

Assuming that your manifest file is named `microservice.yml`, the command:


```sh
kubectl apply -f microservice.yml # i.e. your manifest file
```

 To Do:
 
 _Run the following commands to prepare K8 (minikube) deployment environment.
 
1. Starting minikube (In windows 10 pro). Powershell terminal. 
```sh
 ps>c:\minikube start --driver=hyperv 

```

2. Verifying minikube is ready. 
```sh
  ps> c:\kubectl get nodes
  output:
 NAME       STATUS   ROLES                  AGE   VERSION
minikube   Ready    control-plane,master   53s   v1.21.2
```

3. Clone repository to your local area. 
   
```sh
  ps> c:\mkdir workspace
  ps> c:\cd workspace 
  ps c:\workspace> git clone https://jcrphx@bitbucket.org/jcrphx/simpletimeservice.git
  ps c:\workspace> cd simpletimeservice/app/k8
```

4. Deploying "simpletimeservice.yml" manifest to minikube. 

   
```sh
 ps c:\workspace\simpletimeservice\app\k8> kubectl apply -f .\simpletimeservice.yml
 
 output:
 service/simple-time-service created
 deployment.apps/simple-time-py created
 
```

5. Verifying the pods are running
   
```sh
 ps c:\workspace\simpletimeservice\app\k8> kubectl get pods
   output:
   NAME                              READY   STATUS    RESTARTS   AGE
simple-time-py-67ddb46b95-hmzdq   1/1     Running   0          41s
simple-time-py-67ddb46b95-nz6tg   1/1     Running   0          41s
```

6. Validating Python Web service application deployed in K8 is working as expected
  
```sh
  ps c:\workspace\simpletimeservice\app\k8> minikube service simple-time-service --url
   Output. Example:
   * Starting tunnel for service simple-time-service.
|-----------|---------------------|-------------|------------------------|
| NAMESPACE |        NAME         | TARGET PORT |          URL           |
|-----------|---------------------|-------------|------------------------|
| default   | simple-time-service |             | http://127.0.0.1:50195 |
|-----------|---------------------|-------------|------------------------|

Copy in the browser the URL in the table for example http://127.0.0.1:50195
```sh   
   Response received from the Web application should be similar to this: 
   {"timestamp":"14 December, 2021 at 13:58:18","ip":"172.17.0.1"}
   
```


7. To stop minikube and delete all the resources and services created for this demo
 
```sh
   ps c:\workspace\simpletimeservice\app\k8> minikube delete
```
---

# Task 2 - Terraform: Create and Use a Module
## Create my_eks_cluster module

Create a Terraform module called "my_eks_cluster" with the following specs:

- Takes vpc ID and cluster name as input variables (others may be needed - you're the expert, right?)
- Creates EKS cluster using the VPC ID and cluster name provided by variables
- The cluster must have 2 nodes, using instance type `t3a.large`. 
- The nodes must be on the private subnets only.

Of course, you may use popular public registry modules (e.g. the eks module).

## Use my_eks_cluster module to create EKS cluster

Create a root module in terraform that creates the following resources

- A VPC with 2 public and 2 private subnets
- An EKS cluster using the 'my_eks_cluster' module you created in the previous section


As before, you may use popular public registry modules (e.g. the vpc module)

## Push your code to a public git repo
## Acceptance Criteria

Your task will be considered successful if a colleague is able to deploy infrastructure to AWS and the correct resources are created.

````sh
terraform plan
````
and
````sh
terraform apply
````

must be the only commands needed to create the EKS cluster.

Important Notes:

* Terraform version 1.0.0
* I ran the task #2 from WSL2 terminal (Ubuntu 20.04) 
* The "Acceptance Criteria" passed my testing. 
* The Terraform code used in this task is a variation of a code found during my research, I had not previus experience building AWS EKS       
* Terraform apply and terraform destroy takes approximately 17-20 minutes to complete.
* Issue #1. Due to some errors related to EKS name format reported during "terraform destroy" we only are taking the VPC_ID as input variable. The error is posted bellow, I will continue investigating
  to know how to avoid this issue soon, very interesting considering it is not affecting "Terraform Apply" only "Terraform destroy" process
```sh
- Takes VPC ID and cluster name as input variables
```
Error during "terraform destroy" process: 
```sh
Error: "name" doesn't comply with restrictions ("^[0-9A-Za-z][A-Za-z0-9\\-_]+$"): "-eks-Z59DTKmi"
│
│   with module.eks.aws_eks_cluster.this[0],
│   on .terraform/modules/eks/main.tf line 14, in resource "aws_eks_cluster" "this":
│   14:   name                      = var.cluster_name
│
```
Temporal solution. Run the TF commands
```sh
terraform state rm module.eks.kubernetes_config_map.aws_auth
```
then 
```sh
terraform destroy (again)
```
Reference: 
[https://github.com/terraform-aws-modules/terraform-aws-eks/issues/911]

* Most of the Modules used in this task are public registry modules. They are called from 
```sh
/simpletimeservice/modules/my_eks_cluster/eks-cluster.tf
/simpletimeservice/modules/my_eks_cluster/vpc.tf
```
Information about the files roles located in "my_eks_cluester" directory can be found in README.md located in the same directory

# Prerequisites 
*  Double check AWS CLI unified tool is installed in your local.
```sh
aws --version
Output:
aws-cli/2.4.6 Python/3.8.8 Linux/5.10.60.1-microsoft-standard-WSL2 exe/x86_64.ubuntu.20 prompt/off
```
* AWS IAM Authentication. Make sure that your aws_access_key_id/aws_secret_access_key is setup locally via "aws config" 
* AWS IAM Authorization. Make sure that your credentials are associated to user with the right permission(s) to run the TASK#2. 

# Running the Task
1. Once the SimpletimeService project be cloned, Change to the directory  /simpletimeservice/modules/my_eks_cluster/ 
2. Run the following terraform commands to build and launch the AWS EKS cluster
```sh
  terraform init
  terraform plan 
  terrafor apply ( Enter VPC_ID name )  
```
3. Once validated the EKS Cluster, run the command
```sh
terraform destroy
```